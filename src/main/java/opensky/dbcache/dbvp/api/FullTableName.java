package opensky.dbcache.dbvp.api;


public class FullTableName {
    private String schema;
    private String table;
    private String fullName;

    public FullTableName(String schema, String table) {
        this.schema = schema;
        this.table = table;
        if (schema == null) {
            fullName = table;
        } else {
            if (table == null) {
                fullName = schema;
            } else {
                fullName = schema + "." + table;
            }
        }
    }

    public String getSchema() {
        return schema;
    }

    public String getTable() {
        return table;
    }

    public String getFullName() {
        return fullName;
    }

    @Override
    public int hashCode() {
        int result = 17;
        if (schema != null)
            result = result * 31 + schema.hashCode();
        if (table != null)
            result = result * 31 + table.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof FullTableName) {
            FullTableName t = (FullTableName) obj;
            return equals(schema, t.getSchema()) && equals(table, t.getTable());
        } else {
            return false;
        }
    }

    public static boolean equals(String str1, String str2) {
        if (str1 == null) {
            return str2 == null;
        } else {
            return str2 == null ? false : str1.equals(str2);
        }
    }
    @Override
    public String toString() {
        return fullName;
    }
}
