package opensky.dbcache.dbvp.api;

import java.util.List;
import java.util.Map;

/**
 * 拆分规则集
 *
 * @author shuim
 * @version 1.0
 */
public class RuleSet {

    /**
     * schema和table都是精确匹配的规则列表
     */
    private Map<FullTableName, List<PartionRule>> fullEqualRules;
    /**
     * table是精确匹配,schema是非精确匹配的规则列表
     */
    private Map<String, List<PartionRule>> tableEqualRules;
    /**
     * schema是精确匹配,table是非精确匹配的规则列表
     */
    private Map<String, List<PartionRule>> schemaEqualRules;
    /**
     * schema和table都是非精确匹配的规则列表
     */
    private List<PartionRule> mapRules;

    private RuleSet() {
    }


    public RuleSet(Map<FullTableName, List<PartionRule>> fullEqualRules, Map<String, List<PartionRule>> tableEqualRules,
                   Map<String, List<PartionRule>> schemaEqualRules, List<PartionRule> mapRules) {
        this.fullEqualRules = fullEqualRules;
        this.tableEqualRules = tableEqualRules;
        this.schemaEqualRules = schemaEqualRules;
        this.mapRules = mapRules;
    }

    public Map<FullTableName, List<PartionRule>> getFullEqualRules() {
        return fullEqualRules;
    }

    public void setFullEqualRules(Map<FullTableName, List<PartionRule>> fullEqualRules) {
        this.fullEqualRules = fullEqualRules;
    }

    public Map<String, List<PartionRule>> getTableEqualRules() {
        return tableEqualRules;
    }

    public void setTableEqualRules(Map<String, List<PartionRule>> tableEqualRules) {
        this.tableEqualRules = tableEqualRules;
    }

    public Map<String, List<PartionRule>> getSchemaEqualRules() {
        return schemaEqualRules;
    }

    public void setSchemaEqualRules(Map<String, List<PartionRule>> schemaEqualRules) {
        this.schemaEqualRules = schemaEqualRules;
    }

    public List<PartionRule> getMapRules() {
        return mapRules;
    }

    public void setMapRules(List<PartionRule> mapRules) {
        this.mapRules = mapRules;
    }
}
