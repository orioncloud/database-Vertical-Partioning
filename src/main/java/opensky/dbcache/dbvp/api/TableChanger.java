package opensky.dbcache.dbvp.api;

import java.util.Map;
/**
 * 库表转换器,更具规则转换库表
 *
 * @author shuim
 * @version 1.0
 */
public interface TableChanger {
    /**
     * 转换表名
     *
     * @param name 要转换的表名
     * @param params 上下文参数
     * @return 转换完成的表名,没有转换时返回原表名
     * @throws SqlvpException
     */
    FullTableName change(FullTableName name,Map<String,Object> params);

}
