package opensky.dbcache.dbvp.core;

import opensky.dbcache.dbvp.api.*;
import opensky.dbcache.dbvp.rule.Global;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * 库表转换器,更具规则转换库表
 *
 * @Author zhenggm
 * @Date 2017/9/21 下午4:04
 */
public class TableChangerImpl implements TableChanger {

    private static Logger logger = LoggerFactory.getLogger(TableChangerImpl.class);

    private ErrorMode mode;

    public TableChangerImpl(ErrorMode mode) {
        this.mode = mode;
    }

    @Override
    public FullTableName change(FullTableName table, Map<String, Object> context) {
        logger.debug("开始搜索FullEqualRules,Fulltable为 {}", table.getFullName().toString());
        // 查找RuleSet 匹配规则  若匹配不上则返回原table
        if (Global.RULESET.getFullEqualRules().containsKey(table)) {
            List<PartionRule> fullList = Global.RULESET.getFullEqualRules().get(table);
            for (PartionRule partionRule : fullList) {
                boolean flag = partionRule.map(table);
                if (flag) {
                    // TODO 验证
                    FullTableName resultTable = null;
                    try {
                        resultTable = partionRule.change(table, context);
                    } catch (ParseException e) {
                        // 判断错误模式
                        if (mode.equals(ErrorMode.THROW)) {
                            throw new SqlvpException(e.getMessage());//抛出异常
                        } else {
                            continue;// skip error
                        }
                    }
                    // 如果pass属性是ON，则递归回到当前方法，继续搜索RuleSet。
                    if (partionRule.isPassOn()) {
                        resultTable = change(resultTable, context);
                    }
                    // 该条规则匹配不上继续下一条规则匹配，不等于null表示匹配成功并修改。
                    if (resultTable == null) {
                        continue;
                    }
                    return resultTable;
                } else {
                    continue;
                }
            }
            logger.debug("未匹配到FullEqualRules");
        }

        if (Global.RULESET.getSchemaEqualRules().containsKey(table.getSchema())) {
            logger.debug("开始搜索SchemaEqualRules，Fulltable为 {}", table.getFullName().toString());
            List<PartionRule> fullList = Global.RULESET.getSchemaEqualRules().get(table.getSchema());
            for (PartionRule partionRule : fullList) {
                boolean flag = partionRule.map(table);
                if (flag) {
                    // TODO 验证
                    FullTableName resultTable = null;
                    try {
                        resultTable = partionRule.change(table, context);
                    } catch (ParseException e) {
                        // 判断错误模式
                        if (mode.equals(ErrorMode.THROW)) {
                            throw new SqlvpException(e.getMessage());//抛出异常
                        } else {
                            continue;// skip error
                        }
                    }
                    // 如果pass属性是ON，则递归回到当前方法，继续搜索RuleSet。
                    if (partionRule.isPassOn()) {
                        resultTable = change(resultTable, context);
                    }
                    // 该条规则匹配不上继续下一条规则匹配，不等于null表示匹配成功并修改。
                    if (resultTable == null) {
                        continue;
                    }
                    return resultTable;
                } else {
                    continue;
                }
            }
            logger.debug("未匹配到SchemaEqualRules");
        }

        if (Global.RULESET.getTableEqualRules().containsKey(table.getTable())) {
            List<PartionRule> fullList = Global.RULESET.getTableEqualRules().get(table.getTable());
            for (PartionRule partionRule : fullList) {
                boolean flag = partionRule.map(table);
                if (flag) {
                    // TODO 验证
                    FullTableName resultTable = null;
                    try {
                        resultTable = partionRule.change(table, context);
                    } catch (ParseException e) {
                        // 判断错误模式
                        if (mode.equals(ErrorMode.THROW)) {
                            throw new SqlvpException(e.getMessage());//抛出异常
                        } else {
                            continue;// skip error
                        }
                    }
                    // 如果pass属性是ON，则递归回到当前方法，继续搜索RuleSet。
                    if (partionRule.isPassOn()) {
                        resultTable = change(resultTable, context);
                    }
                    // 该条规则匹配不上继续下一条规则匹配，不等于null表示匹配成功并修改。
                    if (resultTable == null) {
                        continue;
                    }
                    return resultTable;
                } else {
                    continue;
                }
            }
        }

        for (PartionRule partionRule : Global.RULESET.getMapRules()) {
            boolean flag = partionRule.map(table);
            if (flag) {
                // TODO 验证
                FullTableName resultTable = null;
                try {
                    resultTable = partionRule.change(table, context);
                } catch (ParseException e) {
                    // 判断错误模式
                    if (mode.equals(ErrorMode.THROW)) {
                        throw new SqlvpException(e.getMessage());//抛出异常
                    } else {
                        continue;// skip error
                    }
                }
                // 如果pass属性是ON，则递归回到当前方法，继续搜索RuleSet。
                if (partionRule.isPassOn()) {
                    resultTable = change(resultTable, context);
                }
                // 该条规则匹配不上继续下一条规则匹配，不等于null表示匹配成功并修改。
                if (resultTable == null) {
                    continue;
                }
                return resultTable;
            } else {
                continue;
            }
        }

        return table;
    }
}
