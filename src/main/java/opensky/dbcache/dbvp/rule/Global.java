package opensky.dbcache.dbvp.rule;

import opensky.dbcache.dbvp.api.RuleSet;

/**
 * 全局持有静态类
 * @Author zhenggm
 * @Date 2017/9/21 下午4:04
 */
public class Global {
    public static RuleSet RULESET;
}
