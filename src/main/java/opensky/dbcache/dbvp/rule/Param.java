package opensky.dbcache.dbvp.rule;

/**
 * rule属性
 *
 * @Author zhenggm
 * @Date 2017/9/21 下午4:04
 */
public class Param {
    private String vaule;
    private MatchType type;
    private String CLASS; // 数据类型
    private String format; // 日期格式

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public MatchType getType() {
        return type;
    }

    public void setType(MatchType type) {
        this.type = type;
    }

    public String getCLASS() {
        return CLASS;
    }

    public void setCLASS(String CLASS) {
        this.CLASS = CLASS;
    }

    public String getVaule() {
        return vaule;
    }

    public void setVaule(String vaule) {
        this.vaule = vaule;
    }
}
