import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;
import opensky.dbcache.dbvp.api.FullTableName;
import opensky.dbcache.dbvp.api.PartionRule;
import opensky.dbcache.dbvp.core.PartionRuleImpl;
import opensky.dbcache.dbvp.rule.PartionString;
import opensky.dbcache.dbvp.rule.Rule;

import java.util.HashMap;
import java.util.Map;

/**
 * User: zhenggm
 * Date: 2017/9/20 上午9:42
 */
public class BuildImplTest {


    public static void main(String[] args) throws IllegalAccessException, InstantiationException {



        GroovyClassLoader groovyClassLoader = new GroovyClassLoader();
        Class<?> clazz = groovyClassLoader.parseClass(PartionString.getClassContext("return false;"));
        GroovyObject groovyObject = (GroovyObject) clazz.newInstance();

        PartionRule partionRule = (PartionRule) groovyObject;
        Rule rule = new Rule();
        rule.setSchema("12313");
        rule.setTable("1231");
        rule.setFullTableName(new FullTableName("${schema}.000", "${table}-999{age}99"));
        groovyObject.invokeMethod("setRule", rule);
        FullTableName full = new FullTableName("2", "2");
        Map<String, Object> map = new HashMap<>();
        map.put("age", 1);
        Object[] arg = {full, map};
        FullTableName fullTableName = (FullTableName) groovyObject.invokeMethod("change", arg);
        System.out.println(fullTableName);


    }

}
