import opensky.dbcache.dbvp.rule.RegExp;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhenggm
 * @create 2017-09-15 下午 2:25
 **/


public class ParseKeyword {

    // 正则匹配`${}`占位符
    public List<String> getKeywords(String p) {
        String reg = "(?<=(?<!\\\\)\\$\\{)(.*?)(?=(?<!\\\\)\\})";
        RegExp re = new RegExp();
        List<String> list = re.find(reg, p);
        return list;
    }

    public static void main(String[] args) {

        ParseKeyword p = new ParseKeyword();


        String s = "${schema}.13.{name}-{city}{age}{n}";


        Map<String, Object> mapstring = new HashMap<>();
        mapstring.put("name", "小明");
        mapstring.put("age", 12);
        mapstring.put("city", 1L);
        mapstring.put("n",true);
        if (!mapstring.isEmpty() && mapstring != null) {
            for (Map.Entry<String, Object> entry : mapstring.entrySet()) {
                s = s.replace("{" + entry.getKey() + "}", String.valueOf(entry.getValue()));
            }
        }

        List<String> list = p.getKeywords(s);
        for (String n : list) {
            System.out.println(n);
            if (n.equals("schema")) {
                s = s.replace("${schema}", "table");
            }
        }


        System.out.println(s);
        System.out.println(p.getKeywords("${a}a"));
        System.out.println(p.getKeywords("a${a-.a}${a}${a}${a}a"));
        System.out.println(p.getKeywords("a${a\\}a"));
        System.out.println(p.getKeywords("a${a\\}a}a"));
        System.out.println(p.getKeywords("a${a}a${"));
        System.out.println(p.getKeywords("a${ab}a${a}"));
    }
}


