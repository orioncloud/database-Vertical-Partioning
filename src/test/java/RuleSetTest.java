import opensky.dbcache.dbvp.api.FullTableName;
import opensky.dbcache.dbvp.api.MapType;
import opensky.dbcache.dbvp.api.PartionRule;
import opensky.dbcache.dbvp.core.ConfigLoad;
import opensky.dbcache.dbvp.rule.Global;
import opensky.dbcache.dbvp.rule.Rule;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static opensky.dbcache.dbvp.rule.RegExp.match;

/**
 * User: zhenggm
 * Date: 2017/9/19 下午4:39
 */
public class RuleSetTest {

    private static String root = "opensky-dbvp/target/classes/rule.xml";
    private static final Logger logger = LoggerFactory.getLogger(RuleSetTest.class);

    public static void main(String[] args) {

        ConfigLoad configLoad = new ConfigLoad(root);
        FullTableName fullTableName = new FullTableName("user", "age");
        if (Global.RULESET.getSchemaEqualRules().containsKey(fullTableName.getSchema())) {
            List<PartionRule> list = Global.RULESET.getSchemaEqualRules().get(fullTableName.getSchema());
            list.forEach(partionRule -> {
                boolean flag = partionRule.map(fullTableName);
                System.out.println(flag);
            });
        }
        System.out.println(1);

//
//        Rule rule = new Rule();
//        rule.setSchema("user");
//        rule.setSchemaType(MapType.EQUAL);
//        rule.setTable("^[a-z0-9]+$");
//        rule.setTableType(MapType.REG);
//        FullTableName fullTableName = new FullTableName("user", "1A");
//        boolean flag = map(rule,fullTableName);
//        System.out.println(flag);
    }

    public static boolean map(Rule rule, FullTableName table) {
        boolean s = false;
        boolean t = false;
        if (rule.getSchemaType().equals(MapType.EQUAL)) {
            // schema是精确匹配
            s = table.getSchema().equals(rule.getSchema());// 直接比较是否相等
            if (rule.getTableType().equals(MapType.EQUAL)) {
                // table是精确匹配
                t = table.getTable().equals(rule.getTable());
            } else {
                // table是非精确匹配
                // 判断传入table是否匹配规则中的正则表达式
                t = regxStr(table.getTable(), rule.getTable());
            }
        } else {
            // schema是非精确匹配，正则
            s = regxStr(table.getSchema(), rule.getSchema());
            if (rule.getTableType().equals(MapType.EQUAL)) {
                t = table.getTable().equals(rule.getTable());
            } else {
                // 判断传入schema是否匹配规则中的正则表达式
                t = regxStr(table.getTable(), rule.getTable());
            }
        }
        // 只有当s和t都匹配上的时候
        if (s == true && t == true) {
            return true;
        }
        return false;
    }

    private static boolean regxStr(String str, String regx) {
        return match(regx, str);
    }


    @Test
    public void logtest(){

        logger.debug("2132");

    }
}
