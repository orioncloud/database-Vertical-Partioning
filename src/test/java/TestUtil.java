import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.util.deparser.ExpressionDeParser;
import net.sf.jsqlparser.util.deparser.SelectDeParser;
import opensky.dbcache.dbvp.core.DBSelectDeParser;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: zhenggm
 * Date: 2017/9/18 下午2:48
 */
public class TestUtil {

    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
        //groovy提供了一种将字符串文本代码直接转换成Java Class对象的功能
        GroovyClassLoader groovyClassLoader = new GroovyClassLoader();
        //里面的文本是Java代码
        Class<?> clazz = groovyClassLoader.parseClass("package com.xxl.job.core.glue;\n" +
                "import opensky.dbcache.dbvp.rule.Rule;" +
                "public class Main {\n" +
                "\n" +
                "    public int age = 22;\n" +
                "    \n" +
                "    public void sayHello(int a) {\n" +
                "Rule rule = new Rule(); " +
                "rule.setSchema(\"123123\");" +
                "    List list = new ArrayList();" +
                "    list.add(1);" +
                "String s = \"heooool\";" +
                "s = s.replace(\"h\",\"Y\");" +
                "        System.out.println(\"年龄是:\" + s+list.get(0)+rule.getSchema());\n" +
                "    }\n" +
                "}\n");

        GroovyObject groovyObject = (GroovyObject) clazz.newInstance();
        groovyObject.invokeMethod("sayHello", 10000000);
//        Method method = clazz.getDeclaredMethod("sayHello",1);
//        method.invoke(clazz.newInstance());
    }

    @Test
    public void foreach() {

        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);

        list.forEach(i -> {
            if (i == 4) {
                return;
            }
            System.out.println(i);
        });
    }


    @Test
    public void SqlPicker() throws JSQLParserException {
        String sql = "select * from presto.books as t1 left join default.hbooks as t2 on t1.id=t2.id";
        Select select = (Select) CCJSqlParserUtil.parse(sql);

        //Start of value modification
        StringBuilder buffer = new StringBuilder();
        ExpressionDeParser expressionDeParser = new ExpressionDeParser();
        SelectDeParser deparser = new DBSelectDeParser(expressionDeParser, buffer, null, null);
        expressionDeParser.setSelectVisitor(deparser);
        expressionDeParser.setBuffer(buffer);
        select.getSelectBody().accept(deparser);
        //End of value modification
        System.out.println(buffer.toString());
    }
}
